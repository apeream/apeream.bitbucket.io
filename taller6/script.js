function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function completeRule(ruleStr, hex) {
    return '\n' + ruleStr + '{ color: ' + hex + ';}'
}

var colorPallete = ['#FFFFFF',
                    '#FFFFFF',
                    '#FFFFFF',
                    '#FFFFFF',
                    '#FFFFFF'];

function producePalette(hValue, colorGap, saturation, value){
    for (let i = 0; i < 5; i++) {
        var rgb = hsvToRgb(hValue, saturation, value);
        var hex = rgbToHex(Math.trunc(rgb[0]), Math.trunc(rgb[1]), Math.trunc(rgb[2]));
        var colorStr = "#color" + (i + 1).toString();
        colorPallete[i] = hex;
        $(colorStr).css("background-color", hex);
        hValue = hValue + colorGap;
        if (hValue > 1) {
            hValue = hValue - 1;
        }
    }
}

function generateRules(){
    var rulesVal = 
        completeRule('.website-background', colorPallete[0]) +
        '\n' + completeRule('.element-text', colorPallete[1]) +
        '\n' + completeRule('.element-border', colorPallete[2]) +
        '\n' + completeRule('.element-background', colorPallete[3]) +
        '\n' + completeRule('.header', colorPallete[4]);
    $('#css-rules').val(rulesVal);
}

function generatePalette(){
    producePalette(Math.random(), 0.2, 1, 1);
    generateRules();
}

function clearPalette(){
    producePalette(0, 0, 0, 1);
    generateRules();
}
